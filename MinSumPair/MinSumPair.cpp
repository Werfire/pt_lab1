#include "pch.h"
#include "MinSumPair.h"

void min_sum_pair(const size_t size, const int* const seq, size_t minSumPair[])
{
	bool minPos = 0;
	minSumPair[0] = 0;
	minSumPair[1] = 1;

	if(seq[1] < seq[0])
		minPos = 1;

	for(size_t i = 2; i < size; i++)
		if(seq[i] < seq[minSumPair[!minPos]])
		{
			minSumPair[!minPos] = i;
			minPos ^= seq[i] < seq[minSumPair[minPos]];
		}
}
