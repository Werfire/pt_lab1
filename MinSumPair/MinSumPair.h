#pragma once

#ifdef MINSUMPAIR_EXPORTS
#define MINSUMPAIR_API __declspec(dllexport)
#else
#define MINSUMPAIR_API __declspec(dllimport)
#endif

extern "C" MINSUMPAIR_API void min_sum_pair(size_t size, const int* seq, size_t minSumPair[]);
