#include "MinSumPair.h"
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream ifs("seq.txt");
    size_t size;
    ifs >> size;

    if(size < 2)
		throw invalid_argument("Length of the sequence must be at least 2 elements!");

    int *seq = new int[size];

    int curElem;
    for(size_t i = 0; ifs >> curElem; i++)
        seq[i] = curElem;

    ifs.close();
    
    size_t minSumPair[2];

	min_sum_pair(size, seq, minSumPair);

    cout << "The indices of the two smallest elements of the sequence are "
		<< minSumPair[0] << " and " << minSumPair[1] << endl;

    delete[] seq;
}
